<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'travelph');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HG#GC@0TY*_egYH?3<9NoSW)xXD{l1  Rvo;y%6y$q~xQ&!,X69#g5UvxD0<$z^K');
define('SECURE_AUTH_KEY',  ',M/FND;=7VV2-CNXD9W1}gEN5B^nGiC1YoOSUlm+J*o|Le8*<X*KfhEf+(WFYc->');
define('LOGGED_IN_KEY',    '+FH)ZHb,{+( Oeev[9UQNAB-e(7[tnv@N6ka*vCzywJXEeqg5t]q6PM! i nIb/6');
define('NONCE_KEY',        '.0TqP;GfvBu3}O%Y~V<LdoO}Am**gJ*`1Jaw_ZA9!=`kAe((Re,m5_Y~U@6!eI9E');
define('AUTH_SALT',        '`fO3v4kc r4)L2QZx3M`vb*zgO7le^NxsQf@e6iFo[Ru,pcN IlTO$%V[ns5^Tq ');
define('SECURE_AUTH_SALT', 'JK&y6*1=YY_ex|lQRLa>t7?]ejHa6xCR~9&?OWWsS&<y~`/X+1On6fY ~M8F}0g;');
define('LOGGED_IN_SALT',   'N]cdPH%OKS/3/wl{XNKVLmbA!v*0z;|dXTVVihnLvsZhXmW@Squwkp2m1{ZZPlx-');
define('NONCE_SALT',       'E$cbR/sRS@v*r:R|x6)?MTxA:pDkFoGpC@@QIIA&U2+n+`uwL`E(y]~clmOT@6^}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
